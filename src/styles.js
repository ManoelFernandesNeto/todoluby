import styled from "styled-components";

export const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #708090;
`;

export const Input = styled.input.attrs(props => ({
  // podemos definir de forma stática
  type: "text",
  // ou de forma dinâmica
  size: props.size || "1em"
}))`
  color: Black;
  font-size: 1em;
  border: 2px solid #708090;
  border-radius: 3px;
  width: 500px;

  /* Aqui usamos o stylo de forma dinamica, podendo ser alterado no código do react */
  margin: ${props => props.size};
  padding: ${props => props.size};
`;

export const Button = styled.button`
  /* Se eu adicionar a propriedade primary no botão, estas configs serão acrecentadas */
  background: ${props => (props.primary ? "palevioletred" : "white")};
  color: ${props => (props.primary ? "palevioletred" : "white")};

  font-size: 1em;
  margin: 1em;
  height: 55px;
  width: 55px;
  padding: 0.25em 1em;
  border: 1px solid green;
  border-radius: 50%;
  background-color: #2e8b57;
  text-align: center;

  &:hover {
    background-color: #3cb371;
    height: 58px;
    width: 58px;
  }
`;

export const Alert = styled.div`
  color: red;
`;
