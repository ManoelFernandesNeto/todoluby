import React, { Component } from "react";
import { Input, Button, Alert } from "../../styles";
import { Formik } from "formik";
import * as Yup from "yup";
import Error from "../../Error";
import TodoItems from "../TodoItems";

// const validationSchema = Yup.object().shape({
//   tarefa: Yup.string()
//     .min(5, "Deve conter no mínimo 5 caracteres")
//     .max(50, "Deve conter no máximo 10")
//     .required("O campo pertinente a tarefa não pode ficar vazio!")
// });

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tarefa: "",
      items: [],
      errors: false
    };

    this.addItem = this.addItem.bind(this);
  }

  addItem() {
    const { items, tarefa } = this.state;

    if (tarefa.length >= 5) {
      items.push(tarefa);
      this.setState({ ...this.state, tarefa: "" });
      localStorage.setItem("items", items);
      this.setState({ ...this.state, errors: false });
    } else {
      this.setState({ ...this.state, errors: true });
    }
  }

  modificaState(value, id) {
    this.setState({
      ...this.state,
      [id]: value
    });
  }

  render() {
    const { items, tarefa, errors } = this.state;
    return (
      <Formik
        initialValues={(items, tarefa)}
        // validationSchema={validationSchema}
      >
        {({ touched, handleBlur }) => (
          <form>
            {errors === true ? <Alert>No mínimo 5 caracteres</Alert> : ""}
            <Input
              type="text"
              name="tarefa"
              id="tarefa"
              placeholder="Escreva aqui a tarefa a ser realizada"
              onChange={e => this.modificaState(e.target.value, e.target.id)}
              onBlur={handleBlur}
              value={tarefa}
              ref={event => (this._tarefaInput = event)}
            />

            <Button
              type="button"
              title="Clique para adicionar uma nova tarefa"
              onClick={() => this.addItem()}
            >
              +
            </Button>
            <TodoItems lista={items} />
          </form>
        )}
      </Formik>
    );
  }
}

export default TodoList;
