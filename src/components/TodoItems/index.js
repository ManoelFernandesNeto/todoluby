import React, { Component } from "react";

class TodoItems extends Component {
  render() {
    const getItems = localStorage.getItem("items");

    const items = getItems == null ? [] : getItems.split(",");

    return (
      <div>
        <ul>
          {items.map((item, key) => {
            return <li key={key}> {item} </li>;
          })}
        </ul>
      </div>
    );
  }
}

export default TodoItems;
