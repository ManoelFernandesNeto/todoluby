import React, { Component } from "react";
import { Title } from "./styles";

import TodoList from "./components/TodoList";

class App extends Component {
  render() {
    return (
      <div>
        <Title>TodoLuby</Title>
        <TodoList />
      </div>
    );
  }
}

export default App;
